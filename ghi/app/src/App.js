import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './shoes/ShoesList';
import ShoesDetail from './shoes/ShoesDetail';
import ShoeForm from './shoes/ShoeForm';
import HatsList from './hats/HatsList';
import HatDetail from './hats/HatDetail';
import HatForm from './hats/HatForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="shoes/:id" element={<ShoesDetail />} />
          <Route path="shoes/new" element={<ShoeForm />} />
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path=":id" element={<HatDetail />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
