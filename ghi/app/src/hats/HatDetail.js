import { useParams, Link } from 'react-router-dom'
import { useState, useEffect} from 'react'
import './Hats.css';

const HatDetail = () => {
    const [hat, setHat] = useState({})
    const { id } = useParams()

    const getData = async () => {
        const resp = await fetch(`http://localhost:8090/api/hat/${id}`)
        if (resp.ok) {
            const data = await resp.json()
            setHat(data)
        }
    }

    useEffect(()=> {
        getData()
    }, [])

    return <>
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="shadow">
                        <h1 className="title">Hat Details</h1>
                        <div className="row">
                            <div className="picture">
                            {hat.picture_url ? (
                                <img src={hat.picture_url} alt="Hat" />
                                ) : (
                                    <img src="https://us.123rf.com/450wm/pavelstasevich/pavelstasevich1811/pavelstasevich181101028/112815904-no-image-available-icon-flat-vector-illustration.jpg?ver=6" alt="No image available" />
                                )}
                        </div>
                        <div className="col-4 label">Style Name:</div>
                            <div className="col-8 info">{hat.style_name}</div>
                        </div>
                        <div className="row">
                            <div className="col-4 label">Fabric:</div>
                            <div className="col-8 info">{hat.fabric}</div>
                        </div>
                        <div className="row">
                            <div className="col-4 label">Color:</div>
                            <div className="col-8 info">{hat.color}</div>
                        </div>
                        <Link to="/hats/" className="link">Back to Hats</Link>
                    </div>
                </div>
            </div>
        </div>
    </>
}

export default HatDetail;
