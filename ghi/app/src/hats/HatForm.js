import {  Link } from 'react-router-dom'
import React, { useState, useEffect } from "react";

function HatForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState({
    fabric: "",
    style_name: "",
    color: "",
    picture_url: "",
    id: "",
    location_vo_id: null,
  });

  const getData = async () => {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log(formData.location_vo_id);
    const hatsUrl = `http://localhost:8090/api/hats/${formData.location_vo_id}/`;
    console.log(hatsUrl);
    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        fabric: formData.fabric,
        style_name: formData.style_name,
        color: formData.color,
        picture_url: formData.picture_url,
        id: formData.id,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    console.log(fetchConfig);

    const response = await fetch(hatsUrl, fetchConfig);
    console.log(response);

    if (response.ok) {
      setFormData({
        fabric: "",
        style_name: "",
        color: "",
        picture_url: "",
        id: "",
        location_vo_id: null
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    if (inputName === "location") {
      const selectedLocation = locations.find(
        (location) => location.closet_name === value
      );
      if (selectedLocation) { // add error handling for undefined selectedLocation
        console.log(selectedLocation);
        setFormData({
          ...formData,
          [inputName]: value,
          location_vo_id: selectedLocation.id,
        });
      }
    } else {
      setFormData({
        ...formData,
        [inputName]: value,
      });
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input value={formData.fabric} onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.style_name} onChange={handleFormChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture URL" type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.id} onChange={handleFormChange} placeholder="ID" required type="number" name="id" id="id" className="form-control" />
              <label htmlFor="id">ID</label>
            </div>
            <div className="mb-3">
              <select value={formData.location} onChange={handleFormChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.closet_name} value={location.closet_name}>
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
            <Link to="/hats/" className="link">Back to Hats</Link>
          </form>
        </div>
      </div>
    </div>

  );
}
export default HatForm;
