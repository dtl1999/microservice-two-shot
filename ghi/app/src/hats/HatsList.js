import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats');
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setHats(data.hats);
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8090/api/hat/${e.target.id}/`
        console.log(url)

        const fetchConfig = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfig)
        const data = await resp.json()
        console.log(data);

        setHats(hats.filter(h => h.id !== e.target.id))

        getData();
    }

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style Name</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href} value={hat.id}>
                            <td><Link to={`/hats/${hat.id}`}>{ hat.style_name }</Link></td>
                            <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>

                        </tr>
                    )
                })}
            </tbody>
        </table>
        <Link to="/hats/new/" className="link">Create a Hat</Link>
        </>
    )
}

export default HatsList;
