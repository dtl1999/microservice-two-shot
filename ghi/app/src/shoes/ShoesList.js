import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function ShoesList() {

    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/'

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
            console.log(data)

        }
    }

    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/shoes/${e.target.id}/`
        const fetchConfigs = {
            method: "Delete",
            headers: {
                'Content-Type': "application/json"
            }
        }
        console.log(url)
        const response = await fetch(url, fetchConfigs);
        const data = await response.json();
        console.log(data)
        setShoes(shoes.filter(shoe => String(shoe.id) !== e.target.id))
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (<>
        <Link to="/shoes/new"><button className='btn btn-primary'>Create a shoe</button></Link>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Shoe</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td><Link to={`${shoe.id}`}>{shoe.name}</Link></td>
                            <td>{shoe.bin}</td>
                            <td><button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </>
    );
}

export default ShoesList;