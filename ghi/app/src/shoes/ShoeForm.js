import React, { useState, useEffect } from 'react';

const ShoeForm = () => {
    const [formData, setFormData] = useState({
        name: "",
        manufacturer: "",
        color: "",
        picture_url: ""

    })
    const [bins, setBins] = useState([])
    const [bin, setBin] = useState("")
    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
            console.log(data)
        }

    }
    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const handleBinChange = (e) => {
        setBin(e.target.value)
    }

    const handleSumbit = async (e) => {
        e.preventDefault()
        const url = `http://localhost:8080/api/bins/${bin}/shoes/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const resp = await fetch(url, fetchConfig);
        if (resp.ok) {
            const data = await resp.json()
            console.log(data)
            setFormData({
                name: "",
                manufacturer: "",
                color: "",
                picture_url: ""

            });
            setBin("");
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (<>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Shoe</h1>
                    <form id="create-shoe-form" onSubmit={handleSumbit}>
                        <div className="form-floating mb-3">
                            <input required value={formData.name} className="form-control" name="name" id="name" type="text" onChange={handleFormChange} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required value={formData.manufacturer} className="form-control" name="manufacturer" id="manufacturer" type="text" onChange={handleFormChange} />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required value={formData.color} className="form-control" name="color" id="color" type="text" onChange={handleFormChange} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="picture_url">Picture Url</label>
                            <input required value={formData.picture_url} className="form-control" name="picture_url" id="picture_url" type="url" onChange={handleFormChange} />
                        </div>
                        <div className='mb-3'>
                            <select required value={bin} className="form-select" onChange={handleBinChange}>
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.id} value={bin.id}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>);
}
export default ShoeForm;
