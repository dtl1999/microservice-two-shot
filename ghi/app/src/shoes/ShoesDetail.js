import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';

const ShoesDetail = () => {
    const [shoe, setShoe] = useState({})
    const { id } = useParams()
    const fetchData = async () => {
        const url = `http://localhost:8080/api/shoes/${id}`
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setShoe(data)
        }
    }
    const handleDelete = async () => {
        const url = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()

    }
    useEffect(() => {
        fetchData()
    }, [])
    console.log(shoe.picture_url)

    return (<>

        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>{shoe.name}</h1>
                    <h4>Color:  <span>{shoe.color}</span></h4>
                    <h4>Manufacturer: <span>{shoe.manufacturer}</span></h4>
                    <h4><span><img className="card-img-top" src={shoe.picture_url} /></span></h4>

                    <Link to='/shoes' className="btn btn-primary">Return to Shoes List</Link>
                    <div><button onClick={handleDelete} className="btn btn-danger">Delete</button></div>
                </div>
            </div>
        </div>
    </>);


}

export default ShoesDetail