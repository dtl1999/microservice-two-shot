from django.urls import reverse
from django.db import models


class BinVO(models.Model):
    name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200)


class Shoes(models.Model):
    name = models.CharField(max_length=200, default="name")
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"id": self.pk})
