from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href", "id"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["name", "id"]

    def get_extra_data(self, o):
        return {"bin": o.bin.name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["name", "manufacturer", "color", "picture_url", "bin"]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is None:
            shoes = Shoes.objects.all()
        else:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            print(BinVO.objects.all().values())
            print("hi")
            bin = BinVO.objects.get(id=bin_vo_id)
            content["bin"] = bin
            print("*******************", bin)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(name=content["bin"]["name"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Name"},
                status=400,
            )
        Shoes.objects.filter(id=id).update(**content)
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
