# Wardrobify

Team:

* Dan - Im doing hats
* Dominic - I'm doing shoes

## Design

## Shoes microservice
I made models for the properties of shoes then created view code to process the url requests. I got the poller running to populate my BinVO and tied the bin information to the view code as well. After that I created files with the necessary react/jsx code and exported it to my App.js then routed it to the desired urls. 

## Hats microservice

I created a Hat and LocationVO models and integrated them within my views.py file. I then created a poller that would update all instances of location made within wardrope_api to reflect within my hats_api via locationVO. In my views file I created functions that would allow me to create, look at, and delete hat objects and then linked that with our front-end react software to get a fully functioning SPA working.
