from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_http_methods, require_POST
import json
from django.http import JsonResponse
from .models import Hat, LocationVO
from common.json import ModelEncoder



class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        location_vo_id = request.GET.get('location_vo_id')
        if location_vo_id is not None:
            hats = Hat.objects.filter(location_vo_id=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        location_vo_id = int(location_vo_id)
        content = json.loads(request.body)
        try:
            location_href = f'/api/locations/{location_vo_id}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_hat_details(request, hat_id):
    if request.method == "GET":
        hat = get_object_or_404(Hat, pk=hat_id)
        serialized_hat = HatDetailEncoder().default(hat)
        return JsonResponse(
            serialized_hat,
            safe=False
        )
    else:
        count, _ = Hat.objects.filter(id=hat_id).delete()
        return JsonResponse({"Deleted": count > 0})
