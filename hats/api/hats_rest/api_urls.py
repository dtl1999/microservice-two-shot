from django.urls import path
from .views import api_list_hats, api_hat_details

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:location_vo_id>/", api_list_hats, name="api_create_hats"),
    path("hat/<int:hat_id>/", api_hat_details, name="api_hat_details"),
]
