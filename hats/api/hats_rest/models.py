from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=False)
    closet_name = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.closet_name} ({self.import_href})'

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name='hats',
        on_delete=models.CASCADE
    )
